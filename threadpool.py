#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title       : threadpool.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 30-Apr-2012
# Description : Python thread pool class
# : Based on code from posted on Activestate
# : http://code.activestate.com/recipes/577187-python-thread-pool/
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------

from Queue import Queue
from threading import Thread
import sys
import time
import traceback

# -------------------------------------
# Globals
# -------------------------------------

THREADPOOL_DEFAULT_NUM_THREADS = 5

# -------------------------------------
class ThreadPoolWorker(Thread):
	# -------------------------------------
	logger = None
	tasks_queue = None

	# -------------------------------------
	def __init__(self, logger, tasks_queue):
		# -------------------------------------
		# Init
		Thread.__init__(self)

		self.logger = logger
		self.tasks_queue = tasks_queue

		# Start daemon thread
		self.daemon = True
		self.start()

	# -------------------------------------
	def run(self):
		# -------------------------------------
		# Endless loop
		while self.tasks_queue.not_empty:
			try:
				# Fetch item from queue
				if self.tasks_queue:
					func, args, kwargs = self.tasks_queue.get()
					# Execute task
					func(*args, **kwargs)
					# Mark task as completed (remove from queue)
					self.tasks_queue.task_done()
			except Exception as _:
				exc_type, exc_value, exc_traceback = sys.exc_info()
				log_msg = 'Exception = %s :: Stacktrace = %s' % (traceback.format_exception_only(exc_type, exc_value)[0][:-1], traceback.extract_tb(exc_traceback))
				self.logger.logf(level=self.logger.ERROR, module='networkstats', operation='execute', msg=log_msg)

# -------------------------------------
class ThreadPool:
	# -------------------------------------
	logger = None
	num_threads = None
	tasks_queue = None

	# -------------------------------------
	def __init__(self, logger, num_threads=THREADPOOL_DEFAULT_NUM_THREADS):
		# -------------------------------------
		# Init
		self.logger = None
		self.num_threads = num_threads
		self.tasks_queue = Queue(num_threads)

		# Create 'num_threads' worker threads
		for i in range(num_threads):
			ThreadPoolWorker(logger=self.logger, tasks_queue=self.tasks_queue)

	# -------------------------------------
	def __del__(self):
		# -------------------------------------
		self.__close__()

	# -------------------------------------
	def enqueue(self, func, *args, **kwargs):
		# -------------------------------------
		# Add a task to the queue
		self.tasks_queue.put((func, args, kwargs))

	# -------------------------------------
	def wait(self):
		# -------------------------------------
		# Wait for completion of all the tasks in the queue
		self.tasks_queue.join()

	# -------------------------------------
	def close(self):
		# -------------------------------------
		self.__close__()

	# -------------------------------------
	def __close__(self):
		# -------------------------------------
		# Empty the task queue
		self.tasks_queue.empty()
		# Sleep for 1 second to work around Bug #14623 : http://bugs.python.org/issue14623
		time.sleep(1)

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------
	print 'Error : This python script cannot be run as a standalone program.'
