#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : sendmail.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 14-Nov-2014
# Description : Library for sending emails
# -----------------------------------------------------------------------------

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

# -------------------------------------
class Sendmail:
	# -------------------------------------

	APP_NAME = 'sendmail'
	logger = None
	indent = None
	smtp = None

	COMMASPACE = ', '
	ATTACHMENT_FILENAME = 'attachment.txt'

	# -------------------------------------
	def __init__(self, logger, indent):
		# -------------------------------------
		self.logger = logger
		self.indent = indent
		self.smtp = smtplib.SMTP('localhost')

	# -------------------------------------
	def build_body(self, connection, command):
		# -------------------------------------
		body = ""
		return body

	# -------------------------------------
	def send(self, sender_name, sender_email, receivers_to, receivers_cc, receivers_bcc, subject, body, attachments):
		# -------------------------------------
		message = self.__build_message__(sender_name, sender_email, receivers_to, receivers_cc, receivers_bcc, subject, body, attachments)
		try:
			receivers = list(set(receivers_to + receivers_cc + receivers_bcc))
			self.logger.log('%-*s | Sending mail with subject "%s" to [%s]' % (self.indent, self.APP_NAME, subject, ','.join(receivers)), self.logger.STDOUT)
			self.smtp.sendmail(sender_email, receivers, message)
			self.logger.log('%-*s | Mail sent sucessfully' % (self.indent, self.APP_NAME), self.logger.STDOUT)
			return True
		except Exception, e:
			self.logger.log('%-*s | ERROR : %s' % (self.indent, self.APP_NAME, str(e)), self.logger.CRITICAL)
			return False

	# -------------------------------------
	def __build_message__(self, sender_name, sender_email, receivers_to, receivers_cc, receivers_bcc, subject, body, attachments):
		# -------------------------------------
		msg = MIMEMultipart()
		msg['From'] = '%s <%s>' % (sender_name, sender_email)
		if receivers_to:
			msg['To'] = self.COMMASPACE.join(receivers_to)
		if receivers_cc:
			msg['Cc'] = self.COMMASPACE.join(receivers_cc)
		if receivers_bcc:
			msg['Bcc'] = self.COMMASPACE.join(receivers_bcc)
		msg['Subject'] = subject
		msg.attach(MIMEText(body))
		for attachment in attachments:
			attachment_entry = MIMEApplication(attachment['body'])
			attachment_entry['Content-Disposition'] = 'attachment; filename="%s";' % attachment['filename']
			msg.attach(attachment_entry)
		message = msg.as_string()
		return message
