# *Autobot TODO*

* Option to threadify but show results grouped up after execution
* Configuration validation - handle mandatory/optional responses better
* Add multiple commands in single action (commands list)
* Handle JSON parsing errors gracefully
* Handle verbose mode better
* Dynamic indent calculation
