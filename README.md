# *Autobot README*

## Introduction

Autobot is a completely scriptable automation tool which enables you remotely execute commands on multiple host clusters simultaneously. Some of it's important features include: 
 
* Scripted remote command execution
* Parallel execution on multiple hosts
* Remote machine cluster tagging
* Dynamic email content (body/attachment)

## Installing the Dependencies

**python-dev**
    
    $ sudo apt-get install python-dev

**paramiko**
    
    $ sudo pip install paramiko

**pycrypto**
    
    $ sudo pip install pycrypto

**ecdsa**

    $ sudo pip install ecdsa*

**sendmail**
    
    $ sudo apt-get install sendmail

*Optional, only if you want to send emails*

## Installing Autobot

Using the bundled setup.py script, you can create a wrapper shell script which change directory to the code directory so relative links to the configuration files work, and then executes the autobot.py script. This shell script is installed to /usr/bin/ so that it's in your $PATH. 

    $ sudo python setup.py install autobot.py autobot

## Executing Autobot

If you installed the scripts as explained above, you execute the following commands from anywhere.

    $ autobot

Otherwise, you need to navigate to the directory where you installed the files and execute the following command:

    $ cd path/to/autobot    
    $ python autobot.py

Use the *--help* parameter to find out more.

    $ python autobot.py --help
    Usage: autobot.py [options]
    
    Options:
      -h, --help            show this help message and exit
      -v, --verbose         Print status messages
      -c CONFIG, --config=CONFIG
                            CONFIG file to use [default = config/config.json]

## Configuring Autobot

Take a look at the default configuration file (config/config.json), it contains the framework of a basic deployment script. Please note that configuration files are written in strict JSON notation, so make sure to properly escape your script entries and use a JSON validator like [JSONLint](http://jsonlint.com) to validate your configuration file. 

Detailed explanation of the various configuration options:

**General Section**

    "general": {
        "threadify_email": false,
        "paginate_lines": x
    }

This section allows you to define general configuration options, which are fairly self-explanatory.

* *threadify_email*
    * Ensures that the main autobot process isn't blocked on the email sending thread. Valid values are "true" or "false". [Optional, Default=false]
* *paginate_lines*
    * Paginate the output every 'x' lines. Set this to 0, or omit this option to disable pagination. This needs to be an integer value. [Optional, Default=0]

**Variables Section**

    "variables": {
        "VARIABLE_NAME": {
            "value": "$PROMPT$",
            "display": "Display Name",
            "type": "username|password"
        }

This section allows you to define variables which can be used in other parts of the script. A good example would be to specify the working directory, username, or password. See the sample configuration file for a detailed example.

* *VARIABLE_NAME*
    * The variable name to be used. This can be used in other parts of the script by using $VARIABLE_NAME$. [Mandatory]
* *value*
    * The value to be used for the varibale. Use $PROMPT$ if you want to accept this from the user at run-time. [Mandatory]
* *type*
    * Can be "username" or "password". Omit this option altogether if the variable you want to accept from the command line doesn't fall in either bucket. [Optional]
    * *username*
        * Autobot will provide the current logged in user as the default option.
    * *password*
        * Autobot won't echo the password text on the screen.
* *display*
    * Help text to display on the screen when accepting the variable value from the command line if value is $PROMPT$ [Optional, Default=VARIABLE_NAME]

**Predefined Global Variables**

Autobot also provides a set of predefined variables for use in your script. These are accessed the same way as the user-defined variables, i.e. $VARIABLE_NAME$. These can be over-ridden by the user-defined variables explained in the section above.

* *$SUDO_REGEX$*
    * Regex to detect a sudo command. Default value is "\\[sudo\\].*"
* *$LOCAL_USER$*
    * Username of the user executing the Autobot script.
* *$LOCAL_HOSTNAME$*
    * Hostname of the host where the Autobot script is being executed.
* *$TIMESTAMP$*
    * Timestamp (YYYY-MM-DD hh:mm:ss) when the Autobot script was initialized.
* *$SHORT_TIMESTAMP$*
    * Timestamp (YYYYMMDD.hhmmss) when the Autobot script was initialized.
* *$REMOTE_USERNAME$*
    * The username to use when logging on to the remote hosts. By default, it's value is $PROMPT$ unless overridden in the *variables* section.
* *$REMOTE_PASSWORD$*
    * The password to use when logging on to the remote hosts. By default, it's value is $PROMPT$ unless overridden in the *variables* section.

**Remotes Section**

    "remotes": {
        "identifier": {
            "host": "ip_address_or_hostname",
            "port": 22,
            "prompt": ".*@WEB.*\\$ ",
            "tag": "preprod"
        }
    }

* *identifier*
    * Identifier for the host. Give it a meaningful and unique name. It will (potentially) be used in the email section. [Mandatory]
* *host*
    * IP Address or Hostname of the remote host to connect to. [Mandatory]
* *port*
    * Port on the remote host to connect to. [Optional, Default=22]
* *prompt*
    * Regex to use to identify the command prompt on the remote host. This is used by Autobot to detect the end of execution of a single command on the remote shell. [Mandatory]
* *tag*
    * Tag the host as part of a cluster. This is useful when you want to execute different commands on a different set of hosts, or you want to execute the same command on a different set of hosts in different stages (i.e. preprod -> prod deployment). [Mandatory]

**Script Section**

The general outline of the script section is as follows:

    "script": [
        {
            <script_entry>
        },
        {
            <script_entry>
        },
        {
            <script_entry>
        }
    ]

A "script_entry" is defined as follows:

        {
            "display": "Header text to display",
            "text": "Sub text to display",
            "command": "Command to execute",
            "expects": [
                {
					"expect": "Regex pattern to expect in the terminal",
					"response": "String to send to the terminal in response"
				}
            ],
            "email": {
                ...
            },
            "tags": [
                "preprod"
            ],
            "skippable": false,
            "threadify": false,
            "suppress": true
        }

* *display*
    * Header text to display for the current script entry. [Mandatory]
* *text*
    * Detailed text to display for the current script entry. This is only displayed in verbose mode. [Optional]
* *expects*
    * This section enables Autobot to parses the command output and provide a response based on regex match. This can be used for automatic password entries, etc. You can defined multiple expect/response pairs for every script entry. [Optional]
    * *expect*
        * Regex pattern to expect in the terminal output. [Mandatory]
    * *response*
        * String to send to the terminal in response to the matched expect pattern. [Mandatory]
* *email*
    * Used to send emails as part of the Autobot script execution. Explained more in detail in the next section. [Optional]
* *tags*
    * Determines which clusters to execute the command on. Autobot executes the command on all hosts which are tagged with the specified tag. Multiple tags can be specified for the same script entry. [Mandatory]
* *skippable*
    * Adds a [S]kip option in the confirmation prompt. Valid values are "true" or "false". [Optional, Default=false]
* *threadify*
    * Instructs Autobot to execute the current script entry parallelly across all the hosts belonging to the tagged clusters. Valid values are "true" or "false". [Optional, Default=false] 
* *suppress*
    * Suppress the confirmation prompt and terminal output. Useful for initial setup (i.e. cd ~/working/directory). Valid values are "true" or "false". [Optional, Default=false]

A special element that can be defined within a "script_entry" is "email"

            "email": {
                "sender_name": "Autobot",
                "sender_email": "autobot@autobot.com",
                "receivers": {
                    "to": [
                        "devnull.1+to@devnull.com",
                        "devnull.2+to@devnull.com"
                    ],
                    "cc": [
                        "devnull.3+cc@devnull.com",
                    ],
                    "bcc": [
                        "devnull.4+bcc@devnull.com",
                    ]
                },
                "subject": "Email subject",
                "body": {
                    "text": "Email body text",
                    "command": {
                        "command": "Command to build mail body (STDOUT)",
                        "remote": "Identifier of the remote host to execute command on"
                    }
                },
                "attachments": [
                    {
                        "filename": "Attachment filename",
                        "command": {
                            "command": "Command to build mail attachment (STDOUT)",
                            "remote": "Identifier of the remote host to execute command on"
                        }
                    }
                ]
            }

* *sender_name*
    * Display name of the sender. [Mandatory]
* *sender_email*
    * Email ID of the sender. [Mandatory]
* *receivers* 
    * *to*
        * List of Email IDs to send in the "To" field 
    * *cc*
        * List of Email IDs to send in the "Cc" field
    * *bcc*
        * List of Email IDs to send in the "Bcc" field
* *subject*
    * Subject of the email. [Mandatory]
* *body*
    * This section defines the body of the email. The body can either by hard-coded "text", or the STDOUT output of a "command". [Mandatory]
    * *text*
        * Use this option to define a static text email body. This is mutually exclusive with the "command" option described below. [Optional, mutually exclusive with "command"]  
    * *command*
        * Use this option to define a dynamic email body based on the STDOUT output of a command. This is mutually exclusive with the "text" option described above. [Optional, mutually exclusive with "text"]
        * *command*
            * Autobot executes this command, and captures the STDOUT stream for use as the email body. [Mandatory]
        * *remote*
            * Identifier of the remote host where the command is to be executed. [Mandatory]
* *attachments*
    * This section defines the attachments for the email. Use an empty list or omit this option altogether to not have any attachments. 
    * *filename*
        * Defines the attachment file name. [Mandatory]
    * *command*
        * Defines a content of the attachment based on the captured STDOUT output of a command. [Mandatory]
        * *command*
            * Autobot executes this command, and captures the STDOUT stream for use as the email body. [Mandatory]
        * *remote*
            * Identifier of the remote host where the command is to be executed. [Mandatory]
