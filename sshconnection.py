#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : sshconnection.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 12-Nov-2014
# Description : Library for handling SSH connections
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------

import time
import paramiko
from paramikoe import SSHClientInteraction

# -------------------------------------
# Globals
# -------------------------------------

DEFAULT_INDENT = 15
DEFAULT_DEBUG_LEVEL = 0
PAGINATE_SEPARATOR_START = '-' * 5
PAGINATE_SEPARATOR_END = '-' * 5

# --------------------------------------------------------------------------------------------------
class SSHConnection:
	# --------------------------------------------------------------------------------------------------

	remote = None
	variables = None
	logger = None
	indent = None
	debug_level = None

	connected = None
	client = None
	interact = None

	DEFAULT_SSH_PORT = 22
	DEFAULT_BUFFER_SIZE = 1024
	INTERACT_TIMEOUT = 30
	PARAMIKO_LOGFILE = 'logs/paramiko.log'
	SLEEP_TIME = 0.5

	# -------------------------------------
	def __init__(self, remote, variables, logger, indent=DEFAULT_INDENT, debug_level=DEFAULT_DEBUG_LEVEL):
		# -------------------------------------
		# Initialize class variables
		self.remote = remote
		self.variables = variables
		self.logger = logger
		self.indent = indent
		self.debug_level = debug_level

		self.connected = False

		# Fix port
		if 'port' not in self.remote:
			self.remote['port'] = self.DEFAULT_SSH_PORT
		self.remote['port'] = int(self.remote['port'])

		# Initialize paramiko
		paramiko.util.log_to_file(self.PARAMIKO_LOGFILE)
		self.client = paramiko.SSHClient()
		self.client.load_system_host_keys()
		# self.client.set_missing_host_key_policy(paramiko.WarningPolicy)
		self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

	# -------------------------------------
	def connect(self):
		# -------------------------------------
		try:
			self.logger.log('%-*s | Establishing connection : %s@%s:%s' % (self.indent, self.remote['key'], self.variables['REMOTE_USERNAME'], self.remote['host'], self.remote['port']), self.logger.STDOUT)
			self.client.connect(self.remote['host'], self.remote['port'], self.variables['REMOTE_USERNAME'], self.variables['REMOTE_PASSWORD'])
			self.interact = SSHClientInteraction(self.client, timeout=self.INTERACT_TIMEOUT, debug_level=self.debug_level, buffer_size=self.DEFAULT_BUFFER_SIZE)
			self.interact.expect([self.remote['prompt']])
			time.sleep(self.SLEEP_TIME)
			self.connected = True
			self.logger.log('%-*s | Connection : OK' % (self.indent, self.remote['key']), self.logger.STDOUT)
		except Exception, e:
			self.logger.log('%-*s | Connection : ERROR : %s' % (self.indent, self.remote['key'], str(e)), self.logger.CRITICAL)
			self.client = None
			self.connected = False

	# -------------------------------------
	def disconnect(self):
		# -------------------------------------
		if not self.client:
			return

		try:
			self.interact.close()
			if self.client:
				self.client.close()
				self.client = None
				self.connected = False
			self.logger.log('%-*s | Disconnected : OK' % (self.indent, self.remote['key']), self.logger.INFO)
		except Exception, e:
			self.logger.log('%-*s | Disconnected : ERROR : %s' % (self.indent, self.remote['key'], str(e)), self.logger.CRITICAL)
			self.client = None
			self.connected = False

	# -------------------------------------
	def execute(self, cmd, variables=list(), expects=list(), paginate_lines=0, suppress_output=False):
		# -------------------------------------
		response = list()
		if self.client:
			sudo_send = variables['REMOTE_PASSWORD']
			sudo_expect_list = list()
			expect_list = list()


			# Check for expects
			for expect_entry in expects:
				expect_regex = expect_entry['expect']
				time.sleep(self.SLEEP_TIME)
				expect_list.append(expect_regex)

			# Check if sudo
			cmd_has_sudo = False
			if 'sudo ' in cmd:
				cmd_has_sudo = True
				cmd = cmd.replace('sudo ', 'sudo -S ')
				cmd = cmd.replace('sudo -S -S ', 'sudo -S ')
				sudo_expect_list.append(self.variables['SUDO_REGEX'])

			# Send command
			try:
				# Send command
				self.interact.send(cmd)

				# Handle sudo
				if cmd_has_sudo:
					index = self.interact.expect(sudo_expect_list)
					if self.interact.last_match == self.variables['SUDO_REGEX']:
						self.interact.send(sudo_send)
					# index = self.interact.expect([self.remote['prompt']])

				for i in range(len(expect_list)):
					index = self.interact.expect(expect_list)

					# Handle expect
					for expect_entry in expects:
						expect_regex = expect_entry['expect']
						expect_response = expect_entry['response']
						if self.interact.last_match == expect_regex:
							self.interact.send(expect_response)
						# index = self.interact.expect([self.remote['prompt']])

				# Wait for prompt
				index = self.interact.expect([self.remote['prompt']])

			except Exception, e:
				self.logger.log('%-*s | Communication error while sending request : %s' % (self.indent, self.remote['key'], str(e)), self.logger.CRITICAL)
				self.client = None
				return response

			# Read response & write to STDOUT
			try:
				response = self.interact.current_output_clean.split('\n')
				if self.debug_level == 0 and not suppress_output:
					total_number_of_lines = len(response)
					skip_paginate = False
					for current_line_number, line in enumerate(response):
						self.logger.log('%-*s | %s' % (self.indent, self.remote['key'], line), self.logger.STDOUT)
						if paginate_lines > 0 and ((current_line_number + 1) % paginate_lines) == 0 and (current_line_number + 1) != total_number_of_lines and not skip_paginate:
							page_x = int(current_line_number / paginate_lines) + 1
							page_y = int(total_number_of_lines / paginate_lines)
							if total_number_of_lines % paginate_lines != 0:
								page_y += 1
							response = raw_input('%-*s | %s Paginating [%d of %d]. Press [ENTER] to continue or [S] to skip till end %s' % (self.indent, self.remote['key'], PAGINATE_SEPARATOR_START, page_x, page_y, PAGINATE_SEPARATOR_END))
							if response.lower() == 's':
								skip_paginate = True
				elif self.debug_level == 1:
					uncleaned_response = self.interact.current_output.split('\n')
					line = '#' * 25 + '   current_output   ' + '#' * 25
					self.logger.log('%-*s | %s' % (self.indent, self.remote['key'], line), self.logger.STDOUT)
					for line in uncleaned_response:
						self.logger.log('%-*s | %s' % (self.indent, self.remote['key'], line), self.logger.STDOUT)
					line = '#' * 25 + 'current_output_clean' + '#' * 25
					self.logger.log('%-*s | %s' % (self.indent, self.remote['key'], line), self.logger.STDOUT)
					for line in response:
						self.logger.log('%-*s | %s' % (self.indent, self.remote['key'], line), self.logger.STDOUT)
					line = '#' * 25 + '        end         ' + '#' * 25
					self.logger.log('%-*s | %s' % (self.indent, self.remote['key'], line), self.logger.STDOUT)
			except Exception, e:
				self.logger.log('%-*s | Communication error while parsing response : %s' % (self.indent, self.remote['key'], str(e)), self.logger.CRITICAL)
				self.client = None
				return response

		return response
