#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : autobot.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 12-Nov-2014
# Description : Python script to automate deployments
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------

import getpass
import json
import os
import sys
from collections import OrderedDict
from optparse import OptionParser
from time import localtime, strftime

from loghelper import LogHelper
from sendmail import Sendmail
from sshconnection import SSHConnection
from threadpool import ThreadPool

# -------------------------------------
# Globals
# -------------------------------------

options = None
config = None
logger = None
variables = dict()

APP_NAME = 'autobot'

TIMEFORMAT_LOG = '%Y%m%d.%H%M%S'
TIMEFORMAT_DISPLAY = '%Y-%m-%d %H:%M:%S'

LOG_DIR = 'logs'
LOG_FILE_BASENAME = '%s' % (APP_NAME)
LOG_FILE_EXTENSION = 'log'

DEFAULT_CONFIG_FILENAME = 'config/config.json'
DEFAULT_USERNAME = '$PROMPT$'
DEFAULT_PASSWORD = '$PROMPT$'

DEFAULT_PAGINATE_LINES = 0

DEFAULT_OPTIONAL = False
DEFAULT_THREADIFY = False
DEFAULT_SUPPRESS = False

DEFAULT_EMAIL_SENDER_NAME = 'Autobot'
DEFAULT_EMAIL_SENDER_EMAIL = 'autobot@autobot.com'
DEFAULT_EMAIL_RECEIVERS = ['devnull@devnull.com']
DEFAULT_EMAIL_SUBJECT = 'Deployment started @ $TIMESTAMP$'
DEFAULT_EMAIL_BODY = 'Deployment started by $LOCAL_USER$ from $LOCAL_HOST$ at $TIMESTAMP$'

VALID_RESPONSES_YES = ['y', 'yes']
VALID_RESPONSES_NO = ['n', 'no']
VALID_RESPONSES_SKIP = ['s', 'skip']
VALID_RESPONSES_OPTIONAL_FALSE = VALID_RESPONSES_YES + VALID_RESPONSES_NO
VALID_RESPONSES_OPTIONAL_TRUE = VALID_RESPONSES_YES + VALID_RESPONSES_NO + VALID_RESPONSES_SKIP
CONTINUE_MESSAGE_OPTIONAL_TRUE = '[Y]es [N]o [S]kip'
CONTINUE_MESSAGE_OPTIONAL_FALSE = '[Y]es [N]o'

INDENT = 15
SEPARATOR = '-' * 60
NUM_MAIL_THREADS = 3
NEWLINE = '\n'
SUDO_REGEX = '\[sudo\].*'
DEBUG_LEVEL = 0  # 0 = No Debug, 1 = Basic Debug, 2 = Detailed Debug, 3 = Real time output

# -------------------------------------
def parseOptions():
	# -------------------------------------
	usage = 'Usage: %prog [options]'
	parser = OptionParser(usage)

	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='Print status messages')
	parser.add_option('-c', '--config', dest='config', help='CONFIG file to use [default = %default]', metavar='CONFIG', default=DEFAULT_CONFIG_FILENAME)

	(options, args) = parser.parse_args()

	if len(args) != 0:
		parser.print_help()
		sys.exit(1)

	return options

# -------------------------------------
def checkMandatoryFilesExist():
	# -------------------------------------
	flag = True
	mandatoryFiles = [options.config]

	for filename in mandatoryFiles:
		if not (os.path.exists(filename)):
			print '%s | <%s> does not exist' % (APP_NAME, filename)
			flag = False

	if flag == False:
		print '%s | Exiting ...' % (APP_NAME)
		sys.exit(3);

# -------------------------------------
def parseConfig():
	# -------------------------------------
	if os.path.exists(options.config):
		with open(options.config) as json_data_file:
			config = json.load(json_data_file, object_pairs_hook=OrderedDict)
			print '%-*s | Loaded config file <%s>' % (INDENT, APP_NAME, options.config)
			return config

	print '%-*s | Error loading config file <%s>. Exiting ...' % (INDENT, APP_NAME, options.config)
	sys.exit(2)

# -------------------------------------
def build_user_defined_variables():
	# -------------------------------------
	for k, v in config['variables'].items():
		if 'value' in v and v['value'] == '$PROMPT$':
			display = v['display'] if 'display' in v else k
			if 'type' in v and v['type'].lower() == 'username':
				variables[k] = raw_input('%-*s | %s [%s]: ' % (INDENT, APP_NAME, display, getpass.getuser()))
				if variables[k].strip() == '':
					variables[k] = getpass.getuser()
			elif 'type' in v and v['type'].lower() == 'password':
				variables[k] = getpass.getpass('%-*s | %s: ' % (INDENT, APP_NAME, display))
			else:
				variables[k] = raw_input('%-*s | %s: ' % (INDENT, APP_NAME, display))
		else:
			variables[k] = v['value']

# -------------------------------------
def replace_variables(str):
	# -------------------------------------
	for k, v in variables.items():
		str = str.replace('$' + k + '$', v)
	return str

# -------------------------------------
def exit(status_code):
	# -------------------------------------
	# Disconnect remote connections
	logger.log('%-*s | %s' % (INDENT, APP_NAME, SEPARATOR), logger.STDOUT)
	for key, connection in connections.items():
		threadpool.enqueue(connection.disconnect)
	threadpool.wait()

	# Exit
	logger.log('%-*s | Terminating session' % (INDENT, APP_NAME), logger.STDOUT)
	logger.close()
	sys.exit(status_code)

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------

	# Parse command line args
	options = parseOptions()

	# Check if mandatory files exist
	checkMandatoryFilesExist()

	# Parse config file
	config = parseConfig()

	# Start Logging
	timestamp = strftime(TIMEFORMAT_LOG, localtime())
	log_filename = os.path.abspath(os.path.join(LOG_DIR, '%s.%s.%s' % (LOG_FILE_BASENAME, timestamp, LOG_FILE_EXTENSION)))
	logger = LogHelper(log_filename, options.verbose)
	logger.log('%-*s | Initializing new session' % (INDENT, APP_NAME), logger.INFO)
	logger.log('%-*s | (c) 2014 Tushar Saxena (tushar.saxena@gmail.com)' % (INDENT, APP_NAME), logger.STDOUT)

	# Initialize threadpool
	threadpool = ThreadPool(logger=logger, num_threads=len(config['remotes'].items()))
	if 'general' in config and 'threadify_email' in config['general'] and config['general']['threadify_email']:
		mail_threadpool = ThreadPool(logger=logger, num_threads=NUM_MAIL_THREADS)

	# Initialize sendmail
	sendmail = Sendmail(logger, INDENT)

	# General config variables
	paginate_lines = config['general']['paginate_lines'] if 'general' in config and 'paginate_lines' in config['general'] else DEFAULT_PAGINATE_LINES

	# Predefined variables
	variables['SUDO_REGEX'] = SUDO_REGEX
	variables['LOCAL_USER'] = os.environ.get('USER')
	variables['LOCAL_HOSTNAME'] = os.uname()[1]
	variables['TIMESTAMP'] = strftime(TIMEFORMAT_DISPLAY, localtime())
	variables['SHORT_TIMESTAMP'] = strftime(TIMEFORMAT_LOG, localtime())

	# User-defined variables (from config)
	if 'variables' not in config:
		config['variables'] = OrderedDict()
	if 'REMOTE_USERNAME' not in config['variables']:
		config['variables']['REMOTE_USERNAME'] = {'display': 'Remote Server Username', 'type': 'username', 'value': '$PROMPT$'}
	if 'REMOTE_PASSWORD' not in config['variables']:
		config['variables']['REMOTE_PASSWORD'] = {'display': 'Remote Server Password', 'type': 'password', 'value': '$PROMPT$'}
	build_user_defined_variables()

	# Create remote connections
	connections = dict()
	for key, remote in config['remotes'].items():
		remote['key'] = key
		connections[key] = SSHConnection(remote=remote, variables=variables, logger=logger, debug_level=DEBUG_LEVEL)
		threadpool.enqueue(connections[key].connect)
	threadpool.wait()

	# Check authentication status
	authentication_status = True
	for k, v in connections.items():
		if authentication_status and not v.connected:
			status = False
			break
	if authentication_status:
		logger.log('%-*s | Connected to remote hosts' % (INDENT, APP_NAME), logger.STDOUT)
	else:
		exit(4)

	# Loop through script
	for script_entry in config['script']:
		# Calculate flags
		skippable = True if 'skippable' in script_entry and script_entry['skippable'] == True else DEFAULT_OPTIONAL
		threadify = True if 'threadify' in script_entry and script_entry['threadify'] == True else DEFAULT_THREADIFY
		suppress = True if 'suppress' in script_entry and script_entry['suppress'] == True else DEFAULT_SUPPRESS

		if not suppress:
			# Display script header
			logger.log('%-*s | %s' % (INDENT, APP_NAME, SEPARATOR), logger.STDOUT)
			logger.log('%-*s | [%s]' % (INDENT, APP_NAME, replace_variables(script_entry['display'])), logger.STDOUT)
			if 'text' in script_entry:
				logger.log('%-*s | %s' % (INDENT, APP_NAME, replace_variables(script_entry['text'])), logger.STDOUT)
			if 'command' in script_entry:
				logger.log('%-*s | $ %s' % (INDENT, APP_NAME, replace_variables(script_entry['command'])), logger.INFO)
			logger.log('%-*s | %s' % (INDENT, APP_NAME, SEPARATOR), logger.STDOUT)

			# Display prompt & accept response on command line
			response = None
			while (skippable == True and response not in VALID_RESPONSES_OPTIONAL_TRUE) or (skippable == False and response not in VALID_RESPONSES_OPTIONAL_FALSE):
				continue_message = CONTINUE_MESSAGE_OPTIONAL_TRUE if skippable else CONTINUE_MESSAGE_OPTIONAL_FALSE
				response = raw_input('%-*s | Continue? %s : ' % (INDENT, APP_NAME, continue_message)).strip().lower()
			logger.log('%-*s | %s' % (INDENT, APP_NAME, SEPARATOR), logger.STDOUT)

			# Validate response
			if response in VALID_RESPONSES_YES:
				pass
			elif response in VALID_RESPONSES_NO:
				break
			elif response in VALID_RESPONSES_SKIP:
				continue

		# Send email
		if 'email' in script_entry:
			# Sender
			sender_name = replace_variables(script_entry['email']['sender_name'] if 'sender_name' in script_entry['email'] else DEFAULT_EMAIL_SENDER_NAME)
			sender_email = replace_variables(script_entry['email']['sender_email'] if 'sender_email' in script_entry['email'] else DEFAULT_EMAIL_SENDER_EMAIL)

			# Receiver
			receivers_to = script_entry['email']['receivers']['to'] if 'receivers' in script_entry['email'] and 'to' in script_entry['email']['receivers'] else DEFAULT_EMAIL_RECEIVERS
			for index, receiver in enumerate(receivers_to):
				receivers_to[index] = replace_variables(receiver)
			receivers_cc = script_entry['email']['receivers']['cc'] if 'receivers' in script_entry['email'] and 'cc' in script_entry['email']['receivers'] else list()
			for index, receiver in enumerate(receivers_cc):
				receivers_cc[index] = replace_variables(receiver)
			receivers_bcc = script_entry['email']['receivers']['bcc'] if 'receivers' in script_entry['email'] and 'bcc' in script_entry['email']['receivers'] else list()
			for index, receiver in enumerate(receivers_bcc):
				receivers_bcc[index] = replace_variables(receiver)

			# Subject
			subject = replace_variables(script_entry['email']['subject'] if 'subject' in script_entry['email'] else DEFAULT_EMAIL_SUBJECT)

			# Body
			body = DEFAULT_EMAIL_BODY
			if 'body' in script_entry['email'] and 'text' in script_entry['email']['body']:
				# Static Text
				body = replace_variables(script_entry['email']['body']['text'])
			elif 'body' in script_entry['email'] and 'command' in script_entry['email']['body']:
				# Command
				remote = script_entry['email']['body']['command']['remote']
				command = replace_variables(script_entry['email']['body']['command']['command'])
				response = connections[remote].execute(cmd=command, variables=variables, paginate_lines=paginate_lines, suppress_output=True)
				body = NEWLINE.join(response)

			# Attachments
			attachments = list()
			if 'attachments' in script_entry['email']:
				for attachment in script_entry['email']['attachments']:
					attachment_entry = dict()
					attachment_entry['filename'] = replace_variables(attachment['filename'])
					command = replace_variables(attachment['command']['command'])
					remote = attachment['command']['remote']
					response = connections[remote].execute(cmd=command, variables=variables, paginate_lines=paginate_lines, suppress_output=True)
					attachment_entry['body'] = NEWLINE.join(response)
					attachments.append(attachment_entry)
			# Actually send email here
			if 'general' in config and 'threadify_email' in config['general'] and config['general']['threadify_email']:
				mail_threadpool.enqueue(sendmail.send, sender_name, sender_email, receivers_to, receivers_cc, receivers_bcc, subject, body, attachments)
			else:
				sendmail.send(sender_name, sender_email, receivers_to, receivers_cc, receivers_bcc, subject, body, attachments)

		# Execute command
		if 'command' in script_entry:
			for remote in filter(lambda x: x['tag'] in script_entry['tags'], config['remotes'].values()):
				command = replace_variables(script_entry['command'])
				expects = script_entry['expects'] if 'expects' in script_entry else list()
				for expect in expects:
					expect['expect'] = replace_variables(expect['expect'])
					expect['response'] = replace_variables(expect['response'])
				if threadify:
					threadpool.enqueue(connections[remote['key']].execute, command, variables, expects, paginate_lines, suppress)
				else:
					connections[remote['key']].execute(command, variables, expects, paginate_lines, suppress)
			if threadify:
				threadpool.wait()

	# Exit
	exit(0)
