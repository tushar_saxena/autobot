#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : loghelper.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 29-Jun-2011
# Description : Customized wrapper around Python's logging library
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------

import logging
import os

# -------------------------------------
class LogHelper:
	# -------------------------------------

	DEBUG = None
	INFO = None
	STDOUT = None
	WARNING = None
	ERROR = None
	CRITICAL = None

	# -------------------------------------
	def __init__(self, logfile = None, verbose = False):
		# -------------------------------------

		# Check if log_dir exists
		if not os.path.exists(os.path.dirname(logfile)):
			os.makedirs(os.path.dirname(logfile))

		# Create a new logging level (STDOUT) between INFO and WARNING
		logging.STDOUT = 25
		logging.addLevelName(logging.STDOUT, 'STDOUT')

		# Map named logging levels onto self
		self.DEBUG = logging.DEBUG
		self.INFO = logging.INFO
		self.STDOUT = logging.STDOUT
		self.WARNING = logging.WARNING
		self.ERROR = logging.ERROR
		self.CRITICAL = logging.CRITICAL

		# Create a new logging instance
		self.logger = logging.getLogger(logfile)
		self.logger.setLevel(self.DEBUG)

		# Create file handler
		loghandler_file = logging.FileHandler(logfile)
		loghandler_file.setLevel(self.DEBUG)

		# Create console handler
		loghandler_console = logging.StreamHandler()
		if verbose == True:
			loghandler_console.setLevel(logging.INFO)
		else:
			loghandler_console.setLevel(logging.STDOUT)

		# Create formatters
		formatter_file = logging.Formatter(fmt='[%(asctime)10s] [%(levelname)-08s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
		formatter_console = logging.Formatter('%(message)s')

		# Add formatters to handlers
		loghandler_file.setFormatter(formatter_file)
		loghandler_console.setFormatter(formatter_console)

		# Add the handlers to logger
		self.logger.addHandler(loghandler_console)
		self.logger.addHandler(loghandler_file)

	# -------------------------------------
	def log(self, message, level = 0):
		# -------------------------------------

		self.logger.log(level, message)

	# -------------------------------------
	def close(self):
		# -------------------------------------

		logging.shutdown()