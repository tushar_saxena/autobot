#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : setup.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 19-Apr-2012
# Description : [setup] Generic setup utility
# -----------------------------------------------------------------------------

import os
import shutil
import stat
import sys

pwd = None
buildDir = None
targetDir = None
permissions777 = None
permissions755 = None

mode = None
source = None
target = None

# -------------------------------------
def displayUsage():
	# -------------------------------------

	print 'Usage : setup.py install|test <source> <target>'

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------

	targetDir = '/usr/bin/'
	buildDirName = 'build'
	permissions777 = stat.S_IRWXU + stat.S_IRWXG + stat.S_IRWXO
	permissions755 = stat.S_IRUSR + stat.S_IWUSR + stat.S_IXUSR + stat.S_IRGRP + stat.S_IXGRP + stat.S_IROTH + stat.S_IXOTH

	pwd = os.getcwd()
	buildDir = os.path.join(pwd, buildDirName)

	# Check for sudo ---------------------------------------------------------------------
	if os.getuid() != 0:
		print 'Need to run as root!'
		sys.exit(1)

	# Check input parameters -------------------------------------------------------------
	if len(sys.argv) != 4:
		displayUsage()
		sys.exit(2)

	mode = sys.argv[1]
	source = sys.argv[2]
	target = sys.argv[3]

	if mode not in ['install']:
		displayUsage()
		sys.exit(3)

	# Check if source exists -------------------------------------------------------------
	sourceFile = os.path.join(pwd, source)
	if os.path.exists(sourceFile) == False:
		print 'Source file does not exist : %s' % (sourceFile)
		sys.exit(4)

	# Check if build directory exists ----------------------------------------------------
	if os.path.exists(buildDir) == False:
		try:
			os.makedirs(buildDir)
			os.chmod(buildDir, permissions777)
			print 'Created build directory : %s' % (buildDir)
		except Exception, e:
			print 'Error creating build directory : %s' % (buildDir)
			sys.exit(5)

	# Create build file ------------------------------------------------------------------
	buildFile = os.path.join(buildDir, target)

	try:
		buildFileObj = open(buildFile, 'w')
		buildFileObj.write('#!/usr/bin/env bash\n')
		buildFileObj.write('cd %s\n' % (pwd))
		buildFileObj.write('python %s $@\n' % (sourceFile))
		buildFileObj.close()
		os.chmod(buildFile, permissions777)
		print 'Build file created : %s' % (buildFile)
	except Exception, e:
		print 'Error creating build file : %s' % (buildFile)
		sys.exit(6)

	# Copy build file to destination -----------------------------------------------------
	targetFile = os.path.join(targetDir, target)

	try:
		shutil.copyfile(buildFile, targetFile)
		os.chmod(targetFile, permissions755)
		print 'Target file created : %s' % (targetFile)
	except Exception, e:
		print 'Error creating target file : %s' % (targetFile)
		sys.exit(7)

	# Success!  --------------------------------------------------------------------------
	print '%s successfully installed to %s!' % (source, target)
	sys.exit(0)
